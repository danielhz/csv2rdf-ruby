require 'rdf'
require 'rdf/ntriples'

module CSVRDF
  include RDF

  class Translator
    def initialize(iterator, schema)
      @iterator = iterator
      @schema   = schema
      @prefix   = RDF::URI.new(@schema['prefix'])
      @type     = RDF::URI.new(@schema['type'])
      @schemata = []
      @schema['schemata'].each do |column|
        if column.nil?
          @schemata << nil
        else
          predicate = RDF::URI.new(column[0])

          if column.size == 1
            column_parser = Proc.new do |subject, object|
              [subject, predicate, RDF::Literal.new(object)]
            end
          end

          if column.size > 1
            case column[1]
            when 'integer'
              column_parser = Proc.new do |subject, object|
                [subject, predicate, RDF::Literal.new(Integer(object))]
              end
            when 'date'
              column_parser = Proc.new do |subject, object|
                [
                 subject,
                 predicate,
                 RDF::Literal.new(Date.strptime(object, column[2] || '%Y-%m-%d'))
                ]
              end
            end
          end
          
          @schemata << column_parser
        end
      end
    end

    def run(rows, format)
      graph = RDF::Graph.new

      rows.each do |row|
        @iterator = @iterator.next

        resource = @prefix.join(@iterator.to_s)
        
        graph << [resource, RDF.type, @type]

        row.each_index do |i|
          unless @schemata[i].nil? or row[i].nil?
            graph << @schemata[i].call(resource, row[i].strip)
          end
        end
      end

      graph.dump(format)
    end
  end

end
