csvrdf
======

*csvrdf* is a Ruby tool to convert tabular sources (as CSV files)
into RDF files. It have been developed to generate an easy and simple
translation, where every row in the CSV file is translated into a
resource that has a property for each column. This tool is simpler
than [Tarql](http://tarql.github.io/) but less expressive. It aims to
be used by people without SPARQL knowledge.

Installing
----------

```
$ gem install csvrdf
```

Using this application as an script
-----------------------------------

You can use this tool as a command

```
$ csvrdf schema.json data.csv > data.nt
```

where `schema.json` specify how rows are translated and `data.csv` and
and `data.nt` are the input and output files, respectively.

Transformation language
-----------------------

Let us to consider the CSV file `data.csv` that is in the `example`
folder. It was extracted from
[one of the HTML documents](http://web.uchile.cl/transparencia/plantanov2015ab.html)
with salaries of public employees in Chile. Most of the chilean public
institutions are obligated to publish this data in this tabular format.
However, they have small diferences in the columns that are published and in
the format of its values. The goal of csv2rdf is to provide a simple
transformation language that help people to write a translation for each of
the ways that public institutions publish they data. We use JSON as the base
syntax for the transformation language. The transformation of our example CSV
file can be specified as follows:

```javascript
{
    "prefix" : "http://rdf.degu.cl/id/transparencia/uchile/",
    "type"   : "http://rdf.degu.cl/voc/transparencia/Funcionario",
    "schemata": [
        ["http://rdf.degu.cl/voc/transparencia/tipoDeContrato"],
	    ["http://rdf.degu.cl/voc/transparencia/estamento"],
	    ["http://rdf.degu.cl/voc/transparencia/apellidoPaterno"],
	    ["http://rdf.degu.cl/voc/transparencia/apellidoMaterno"],
	    ["http://rdf.degu.cl/voc/transparencia/nombres"],
	    ["http://rdf.degu.cl/voc/transparencia/grado"],
	    ["http://rdf.degu.cl/voc/transparencia/calificación"],
	    ["http://rdf.degu.cl/voc/transparencia/función"],
	    ["http://rdf.degu.cl/voc/transparencia/región"],
	    null,
	    ["http://rdf.degu.cl/voc/transparencia/unidadMonetaria"],
	    ["http://rdf.degu.cl/voc/transparencia/remuneraciónBrutaMensualizada", "integer"],
	    null,
	    null,
	    ["http://rdf.degu.cl/voc/transparencia/fechaInicio", "date", "%d/%m/%Y"],
	    ["http://rdf.degu.cl/voc/transparencia/fechaTérmino", "date", "%d/%m/%Y"],
	    null
    ]
}
```

The attribute `'prefix'` define the prefix of the resource URI used for each
row in the CSV file. By default, the suffix is generated with a numeric
iterator.

The attribute `'type'` is the type of the resources that will be created for
all rows.

The attribute `'schemata'` is a list with the schema of the columns that
will be parsed. `null` elements in the schemata represent columns that will
be ignored.

The schema of columns is represented with arrays. The first component
must be an URI for predicate of the column and the second the datatype
of the column. Currently, supported datatypes are `'integer'` and `'date'`.
If the datatype is ommited, then an string literal is assumed.

The `'date'` datatype allows an optional argument that is the format
(according to the Ruby method Data#strptyme) used to parse the date.
By default the format is `'%Y-%m-%d'`.

Licencing under CC0-1.0 Public Domain Dedication
------------------------------------------------

The person who associated a work with this deed has dedicated the work
to the public domain by waiving all of his or her rights to the work
worldwide under copyright law, including all related and neighboring
rights, to the extent allowed by law.

You can copy, modify, distribute and perform the work, even for
commercial purposes, all without asking permission.
[See Other Information](https://creativecommons.org/publicdomain/zero/1.0/).

