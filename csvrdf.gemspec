Gem::Specification.new do |s|
  s.name        = 'csvrdf'
  s.version     = '0.0.2'
  s.add_runtime_dependency "rdf", "~> 1.99"
  s.executables << 'csvrdf'
  s.date        = '2016-01-10'
  s.summary     = "CSV to RDF"
  s.description = "A simple translator of CSV files into RDF"
  s.authors     = ["Daniel Hernandez"]
  s.email       = 'daniel@degu.cl'
  s.files       = ["lib/csvrdf.rb"]
  s.homepage    = 'https://gitlab.com/danielhz/csv2rdf-ruby'
  s.license     = 'CC0-1.0'
end